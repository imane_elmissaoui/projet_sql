----Requête 1 :Afficher les patients nés à partir du premier janvier 1990----

select * 
from patient 
where date_naissance >   '1990-01-01';

---Requête 2 : Afficher les patients nés à partir du premier janvier 1990 et de sexe féminin----

select *
from patient
where date_naissance > '1990-01-01'
and sexe = 'F';

---Requête 3 :Afficher les patients nés à partir du premier janvier 1990, de sexe féminin 
---et résidant dans les villes avec id 1, 3 et 5---

select *
from patient
where date_naissance > '1990-01-01'
and sexe = 'F' and id_ville in (1,3,5);

--Requête 4 :Afficher les patients résidant dans les villes Lille et Roubaix. Pour cette requête, ne pas utiliser
--directement les identifiants des villes mais plutôt leurs noms (Lille et Roubaix)---

select id_patient,date_naissance,sexe,ville
from patient p inner join ville v 
on p.id_ville = v.id_ville 
where v.ville in ('Lille','Roubaix');

--Requête 5 :Compter le nombre de patients résidant dans les villes Lille et Roubaix (pour chacune des villes).
--Pour cette requête, ne pas utiliser directement les identifiants des villes mais plutôt leurs noms (Lille
--et Roubaix)--
select v.ville, count(*) as nombre_patients
from patient p inner join ville v 
on p.id_ville = v.id_ville
where v.ville ='Lille' or v.ville = 'Roubaix'
group by v.ville 
order by count(*) desc;

--Requête 6:Compter le nombre de séjours débutant chaque jour, en les triant par date de début.
select date_debut_sejour, count(*) as nombre_de_sejours
from sejour 
group by date_debut_sejour
order by date_debut_sejour desc;

--Requête 7 :Compter le nombre de séjours débutant chaque jour, en les triant par date de début et en ne
--conservant que les jours ayant plus de 2 séjours.--

select date_debut_sejour, count(*) as nombre_de_sejours
from sejour 
group by date_debut_sejour
having count(*) > 2
order by date_debut_sejour desc;

--Requête 8 : Afficher les RUMs du patient n°1--

select p.id_patient,id_rum
from patient p
join sejour s 
on p.id_patient = s.id_patient 
join rum r 
on s.id_sejour = r.id_sejour 
where p.id_patient  = 1;

--Requête 9 :Insérer 3 RUMs pour le séjour n°11. Vous pouvez choisir les dates, modes d'admission et mode de
--sortie. Le tout doit être cohérent--

insert into rum (id_rum, id_sejour, date_entree, date_sortie, mode_admission, mode_sortie)
values
(6, 11, '2020-01-03', '2020-01-05', '5', '5'),
(7, 11, '2020-01-04', '2020-01-10', '5', '8'),
(8, 11, '2020-01-05', '2020-01-06', '8','8');


--Requête 10 :Insérer 1 acte pour chacun des RUMs du séjour n°11

-- on a 3 id_rum pour id_sej 11 : 6,7 et 8
INSERT INTO rum_acte (id_rum, id_acte, date_acte)
 VALUES
 (6, 1, '2020-01-03'),
 (7, 2, '2020-01-06'),
 (8, 3, '2020-01-04');


--Requête 11 :Insérer 1 diagnostic pour chacun des RUMs du séjours n°11
INSERT INTO rum_diagnostic (id_rum, id_diagnostic, date_diagnostic)
 VALUES
 (6, 4, '2020-01-02'),
 (7, 3, '2020-01-04'),
 (8, 5, '2020-01-03');
 
--Requête 12 :Créer une table MEDICAMENT permettant de stocker une liste de médicaments
CREATE TABLE MEDICAMENT (
	id_medicament 		int NOT NULL,
	nom_medicament	            VARCHAR(50) not null,
    constraint pk_medicament primary key (id_medicament)
    );
	
   
 --Enregistrements dans la table MEDICAMENT 
 INSERT INTO MEDICAMENT (id_medicament,nom_medicament)
 VALUES
 (1,'Parecetamol'),
 (4,'Amoxicilline'),
 (2,'Doliprane'),
 (3, 'Imodium'),
 (5,'Metformine');

--Requête 13 :Créer une table RUM_MEDICAMENT permettant de stocker des administrations de médicaments
--lors de RUM--
CREATE TABLE RUM_MEDICAMENT (
    id_administration_medicament int not null,
    id_rum              int not null,
    id_sejour           int not null,
	id_medicament 		int NOT NULL,
	nom		            VARCHAR(50) not NULL,
	constraint pk_rummedicament primary key (id_administration_medicament),
	constraint fk_rum_medicament foreign key (id_rum) references rum (id_rum),
	constraint fk_sejour_rummedicament foreign key (id_sejour) references sejour(id_sejour),
    constraint fk_medicament_rummedicament foreign key (id_medicament) references MEDICAMENT(id_medicament)
   );
--Requête 14 :Insérer 5 administrations de médicaments dans la table RUM_MEDICAMENT, pour le séjour 11
INSERT INTO RUM_MEDICAMENT (id_administration_medicament,id_rum,id_sejour,id_medicament,nom)
 VALUES
 (1, 6, 11, 1, 'Dafalgan'),
 (2, 7, 11, 4, 'Amoxicilline'),
 (3, 8, 11, 2, 'Doliprane'),
 (4, 6, 11, 3, 'Imodium'),
 (5, 8, 11, 5,'Metformine');
